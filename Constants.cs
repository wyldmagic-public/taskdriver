﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using Spectre.Console;
using TaskDriver.Models;

internal static class Constants
{
    internal const string AppName = "Task Driver";
    internal const string AppVersion = "0.0.1";
    internal const string AppDescription = "A simple task priority application";
    internal const string AppAuthor = "Joshua Allen";
    internal const string AppAuthorEmail = "joshua.j.allen@live.com";
    internal const string AppFolderName = ".taskdriver";
    internal const string AppDatabaseFilename = "Entries.db";
    internal static readonly string AppProjectPath = @$"./{AppFolderName}";
    internal static readonly string AppDatabasePath = @$"{AppProjectPath}/{AppDatabaseFilename}";

    internal static Text FormatPriorityText(Priority priority)
    {
        var lColor = priority switch
        {
            Priority.Low => Color.Green,
            Priority.Medium => Color.Blue,
            Priority.High => Color.Yellow,
            Priority.Critical => Color.Red,
            _ => Color.Blue
        };

        return new Text(priority.ToString(), new Style(lColor));
    }

    internal static string FormatPriorityMarkdown(Priority priority)
    {
        var lColor = priority switch
        {
            Priority.Low => "[green]{0}[/]",
            Priority.Medium => "[cornflowerblue]{0}[/]",
            Priority.High => "[yellow]{0}[/]",
            Priority.Critical => "[red]{0}[/]",
            _ => "[blue]{0}[/]"
        };

        return string.Format(lColor, priority.ToString());
    }
}