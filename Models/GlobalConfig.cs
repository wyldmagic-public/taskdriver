﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using TaskDriver.Models.Abstractions;

namespace TaskDriver.Models;

internal sealed class GlobalConfig : ConfigBase
{
    public GlobalConfig() { }

    public GlobalConfig(string name, string email)
    {
        Name = name;
        Email = email;
    }

    public override string ConfigType => nameof(GlobalConfig);

    public string Name { get; set; }

    public string Email { get; set; }
}