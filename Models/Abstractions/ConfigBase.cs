﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using LiteDB;

namespace TaskDriver.Models.Abstractions;

internal abstract class ConfigBase
{
    public ObjectId _id { get; set; }

    public abstract string ConfigType { get; }
}