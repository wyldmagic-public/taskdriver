﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

namespace TaskDriver.Models;

internal enum Priority
{
    Low,
    Medium,
    High,
    Critical
}