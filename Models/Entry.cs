﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using LiteDB;

namespace TaskDriver.Models;

internal sealed class Entry
{
    public Entry(string title, Priority priority = Priority.Medium, string owner = default)
    {
        Title = title;
        Owner = owner;
        Priority = priority;
    }

    public Entry(string title, Priority priority = Priority.Medium, string owner = default, params string[] tags)
        : this(title, priority, owner)
    {
        if (false == ReferenceEquals(null, tags) && tags.Length > 0) Tags.AddRange(tags);
    }

    public ObjectId _id { get; set; }

    public bool Completed { get; set; }

    public string Title { get; set; }

    public string Owner { get; set; }

    public Priority Priority { get; set; }

    public List<string> Tags { get; set; } = [];
}