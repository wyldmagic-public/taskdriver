# TaskDriver

TaskDriver is a simple task priority application

![TaskDriver Screenshot](https://web-wyldmagic-public.s3.amazonaws.com/TaskDriver+example.png)

## Installation

You can publish the project from source or download the win_x64 binaries. Regardless, make sure the binary is placed in a location registered in the PATH environment variable of your system.

## Optional

You may want to register an alias for the TaskDriver executable to your .bash_profile

```
alias td='TaskDriver'
```

## Usage

```
USAGE:
    TaskDriver.exe [OPTIONS] <COMMAND>

EXAMPLES:
    TaskDriver.exe init Joshua
    TaskDriver.exe init Joshua joshua@example.com
    TaskDriver.exe add "My first task"
    TaskDriver.exe add "My second task" -o "Joshua Allen"
    TaskDriver.exe add "My second task" -p "High"

OPTIONS:
    -h, --help    Prints help information

COMMANDS:
    init       Initializes or updates a TaskDriver project in the current directory
    add        Adds a new entry to the TaskDriver project
    list       Lists or searches entries in the TaskDriver project
    update     Updates an entry in the TaskDriver project
    delete     Deletes an entry from the task driver project
    details    Shows a report of the TaskDriver project
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)