﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using System.ComponentModel;
using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;

namespace TaskDriver.Commands;

internal sealed class UpdateEntryCommand : Command<UpdateEntryCommand.Settings>
{
    internal sealed class Settings : CommandSettings
    {
        [CommandArgument(0, "[id]")]
        public string Id { get; set; }

        [CommandOption("-c|--complete")]
        [Description("Marks the entry as complete")]
        public bool? Complete { get; set; }

        [CommandOption("-t|--title <title>")]
        [Description("Sets the title of the entry")]
        public string Title { get; set; }

        [CommandOption("-o|--owner <owner>")]
        [Description("Sets the owner of the entry")]
        public string Owner { get; set; }

        [CommandOption("-p|--priority <priority>")]
        [Description("Sets the priority of the entry")]
        public Priority? Priority { get; set; }

        [CommandOption("-a|--add-tags <tags>")]
        [Description("Adds the given tags to the entry")]
        public string[] AddTags { get; set; } = [];

        [CommandOption("-r|--remove-tags <tags>")]
        [Description("Removes the given tags from the entry")]
        public string[] RemoveTags { get; set; } = [];
    }

    private readonly ILiteDatabase _db;

    public UpdateEntryCommand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        if (string.IsNullOrWhiteSpace(settings.Id)) settings.Id = AnsiConsole.Ask<string>("[blue]What is the Id of your entry?[/]");
        settings.Id = Markup.Remove(settings.Id);

        if (string.IsNullOrWhiteSpace(settings.Id))
            return -1;

        var lEntries = _db.GetCollection<Entry>(nameof(Entry));

        var lEntry = lEntries.FindById(new ObjectId(settings.Id));
        if (ReferenceEquals(null, lEntry))
        {
            AnsiConsole.MarkupLine($"[red]Entry not found:[/] [white]{settings.Id}[/]");
            return -1;
        }

        if (false == string.IsNullOrWhiteSpace(settings.Title)) lEntry.Title = settings.Title;
        else
        {
            settings.Title = AnsiConsole.Ask<string>("[blue]Title:[/]", lEntry.Title);
            settings.Title = Markup.Remove(settings.Title);
            lEntry.Title = settings.Title;
        }

        if (false == string.IsNullOrWhiteSpace(settings.Owner)) lEntry.Owner = Markup.Remove(settings.Owner);
        if (settings.Complete.HasValue) lEntry.Completed = settings.Complete.Value;
        if (settings.Priority.HasValue) lEntry.Priority = settings.Priority.Value;

        if (settings.AddTags.Length > 0)
        {
            var lAdd = settings.AddTags
                .Select(Markup.Remove)
                .Where(x => false == lEntry.Tags.Contains(x));
            lEntry.Tags.AddRange(lAdd);
        }

        if (settings.RemoveTags.Length > 0)
        {
            lEntry.Tags.RemoveAll(x => settings.RemoveTags.Select(Markup.Remove).Contains(x));
        }

        if (false == lEntries.Update(lEntry))
        {
            AnsiConsole.MarkupLine($"[red]Entry could not be updated:[/] [white]{settings.Id}[/]");
            return -1;
        }

        var lGrid = new Grid();
        lGrid.AddColumns(7);

        lGrid.AddRow(new Text[]
        {
            new Text("Created", new Style(Color.White)),
            new Text("Id", new Style(Color.White)),
            new Text("Completed", new Style(Color.White)),
            new Text("Title", new Style(Color.White)),
            new Text("Priority", new Style(Color.White)),
            new Text("Owner", new Style(Color.White)),
            new Text("Tags", new Style(Color.White))
        });

        lGrid.AddRow(new Text[]
        {
            new Text(lEntry._id.CreationTime.ToLocalTime().ToString(), new Style(Color.Yellow)),
            new Text(lEntry._id.ToString(), new Style(Color.White)),
            new Text(lEntry.Completed.ToString(), new Style(lEntry.Completed ? Color.Green : Color.Red)),
            new Text(lEntry.Title, new Style(Color.Blue)),
            Constants.FormatPriorityText(lEntry.Priority),
            new Text(lEntry.Owner ?? string.Empty, new Style(Color.Yellow)),
            new Text(string.Join(", ", lEntry.Tags), new Style(Color.White))
        });

        AnsiConsole.MarkupLine($@"[blue]Updated entry:[/]");
        AnsiConsole.Write(lGrid);

        return 0;
    }
}