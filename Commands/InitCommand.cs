﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;
using TaskDriver.Models.Abstractions;

namespace TaskDriver.Commands;

internal sealed class InitCommand : Command<InitCommand.Settings>
{
    internal sealed class Settings : CommandSettings
    {
        [CommandArgument(0, "[name]")]
        public string Name { get; set; }

        [CommandArgument(1, "[email]")]
        public string Email { get; set; }
    }

    private readonly ILiteDatabase _db;

    public InitCommand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        AnsiConsole.MarkupLine("[blue]Initializing Task Driver project...[/]");

        var lConfigs = _db.GetCollection<GlobalConfig>("Config");
        var lConfig = lConfigs.FindOne(x => x.ConfigType == nameof(GlobalConfig));

        if (string.IsNullOrWhiteSpace(settings.Name))
            settings.Name = AnsiConsole.Ask<string>("[blue]What is your name?[/]", lConfig?.Name);

        if (string.IsNullOrWhiteSpace(settings.Email))
            settings.Email = AnsiConsole.Ask<string>("[blue]What is your email?[/]", lConfig?.Email);

        lConfig ??= new GlobalConfig();

        if (false == string.IsNullOrWhiteSpace(settings.Name)) lConfig.Name = Markup.Remove(settings.Name);
        if (false == string.IsNullOrWhiteSpace(settings.Email)) lConfig.Email = Markup.Remove(settings.Email);

        lConfigs.Upsert(lConfig);

        var lGrid = new Grid();
        lGrid.AddColumns(2);

        lGrid.AddRow(new[]
        {
            new Text("Name", new Style(Color.White)),
            new Text("Email", new Style(Color.White))
        });

        lGrid.AddRow(new[]
        {
            new Text(lConfig.Name, new Style(Color.Yellow)),
            new Text(lConfig.Email, new Style(Color.Yellow))
        });

        AnsiConsole.MarkupLine($"[blue]Initialized Task Driver project:[/]");
        AnsiConsole.Write(lGrid);
        return 0;
    }
}