﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using System.ComponentModel;
using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;

namespace TaskDriver.Commands;

internal sealed class AddEntryCommand : Command<AddEntryCommand.Settings>
{
    internal sealed class Settings : CommandSettings
    {
        [CommandArgument(0, "[title]")]
        public string Title { get; set; }

        [CommandOption("-o|--owner <owner>")]
        [Description("Sets the owner of the entry")]
        public string Owner { get; set; }

        [CommandOption("-p|--priority <priority>")]
        [Description("Sets the priority of the entry")]
        public Priority? Priority { get; set; }

        [CommandOption("-t|--tags <tags>")]
        [Description("Sets the tags of the entry")]
        public string[] Tags { get; set; }
    }

    private readonly ILiteDatabase _db;

    public AddEntryCommand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        if (string.IsNullOrWhiteSpace(settings.Title)) settings.Title = AnsiConsole.Ask<string>("[blue]What is the title of your entry?[/]");
        settings.Title = Markup.Remove(settings.Title);

        if (string.IsNullOrWhiteSpace(settings.Title))
            return -1;

        var lConfigs = _db.GetCollection<GlobalConfig>("Config");
        var lEntries = _db.GetCollection<Entry>(nameof(Entry));

        var lConfig = lConfigs.FindOne(x => x.ConfigType == nameof(GlobalConfig));
        if (ReferenceEquals(null, lConfig))
        {
            AnsiConsole.MarkupLine("[red]Please initialize the project first.[/]");
            return -1;
        }
        var lEntry = new Entry(settings.Title, settings.Priority ?? Priority.Medium, Markup.Remove(settings.Owner ?? lConfig?.Name), settings.Tags);

        lEntries.Insert(lEntry);

        var lGrid = new Grid();
        lGrid.AddColumns(7);

        lGrid.AddRow(new Text[]
        {
            new Text("Created", new Style(Color.White)),
            new Text("Id", new Style(Color.White)),
            new Text("Completed", new Style(Color.White)),
            new Text("Title", new Style(Color.White)),
            new Text("Priority", new Style(Color.White)),
            new Text("Owner", new Style(Color.White)),
            new Text("Tags", new Style(Color.White))
        });

        lGrid.AddRow(new Text[]
        {
            new Text(lEntry._id.CreationTime.ToLocalTime().ToString(), new Style(Color.Yellow)),
            new Text(lEntry._id.ToString(), new Style(Color.White)),
            new Text(lEntry.Completed.ToString(), new Style(lEntry.Completed ? Color.Green : Color.Red)),
            new Text(lEntry.Title, new Style(Color.Blue)),
            Constants.FormatPriorityText(lEntry.Priority),
            new Text(lEntry.Owner ?? string.Empty, new Style(Color.Yellow)),
            new Text(string.Join(", ", lEntry.Tags), new Style(Color.White))
        });

        AnsiConsole.MarkupLine($"[blue]Added entry:[/]");
        AnsiConsole.Write(lGrid);

        return 0;
    }
}