﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using System.ComponentModel;
using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;

namespace TaskDriver.Commands;

internal sealed class ShowDetailsCommmand : Command<ShowDetailsCommmand.Settings>
{
    private readonly Text[] Columns = new []
    {
        new Text("Owner", new Style(Color.White)),
        new Text("Completed", Color.White),
        new Text("Outstanding", Color.White),
    };

    internal sealed class Settings : CommandSettings
    {
    }

    private readonly ILiteDatabase _db;

    public ShowDetailsCommmand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        var lEntries = _db.GetCollection<Entry>(nameof(Entry));
        var lResults = lEntries.FindAll();
        if (false == lResults.Any())
        {
            AnsiConsole.MarkupLine("[blue]No entries found[/]");
            return 0;
        }

        var lGrid = new Grid();
        lGrid.AddColumns(Columns.Length);
        lGrid.AddRow(Columns);

        var lOwners = new Dictionary<string, List<Entry>>();

        foreach (var lEntry in lResults)
        {
            if (false == lOwners.ContainsKey(lEntry.Owner))
            {
                lOwners[lEntry.Owner] = new List<Entry>();
            }

            lOwners[lEntry.Owner].Add(lEntry);
        }

        foreach (var lOwner in lOwners.Keys)
        {
            lGrid.AddRow(new Text[]
            {
                new Text(lOwner, new Style(Color.White)),
                new Text(lOwners[lOwner].Count(x => x.Completed).ToString(), new Style(Color.Green)),
                new Text(lOwners[lOwner].Count(x => false == x.Completed).ToString(), new Style(Color.Red)),
            });

            lGrid.AddEmptyRow();
        }

        AnsiConsole.Write(lGrid);

        return 0;
    }
}