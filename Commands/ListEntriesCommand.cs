﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using System.ComponentModel;
using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;

namespace TaskDriver.Commands;

internal sealed class ListEntriesCommand : Command<ListEntriesCommand.Settings>
{
    internal sealed class Settings : CommandSettings
    {
        [CommandOption("-a|--all")]
        public bool All { get; set; }

        [CommandOption("-f|--filter <filter>")]
        public string Filter { get; set; }

        [CommandOption("-o|--order <order>")]
        [DefaultValue("desc")]
        public string Order { get; set; }

        [CommandOption("-t|--tags <tags>")]
        public string[] Tags { get; set; } = [];
    }

    private readonly ILiteDatabase _db;

    public ListEntriesCommand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        IEnumerable<Entry> lResults = Array.Empty<Entry>();

        var lEntries = _db.GetCollection<Entry>(nameof(Entry));

        var lQuery = lEntries.Query()
            .Where(x => settings.All || false == x.Completed)
            // .Where(x => settings.Tags.Length == 0 || x.Tags.Any(t => settings.Tags.Contains(t)))
            .Where(x => string.IsNullOrWhiteSpace(settings.Filter) || x.Title.Contains(settings.Filter));

        if (string.IsNullOrWhiteSpace(settings.Order) || settings.Order.ToLower() == "desc")
        {
            lQuery = lQuery.OrderByDescending(x => x._id.CreationTime);
        }
        else if (settings.Order.ToLower() == "asc")
        {
            lQuery = lQuery.OrderBy(x => x._id.CreationTime);
        }

        lResults = lQuery.ToList();

        if (false == lResults.Any())
        {
            AnsiConsole.MarkupLine("[blue]No entries found[/]");
            return 0;
        }

        var lGrid = new Grid();
        lGrid.AddColumns(7);

        lGrid.AddRow(new Text[]
        {
            new Text("Created", new Style(Color.White)),
            new Text("Id", new Style(Color.White)),
            new Text("Completed", new Style(Color.White)),
            new Text("Title", new Style(Color.White)),
            new Text("Priority", new Style(Color.White)),
            new Text("Owner", new Style(Color.White)),
            new Text("Tags", new Style(Color.White))
        });

        foreach (var lEntry in lResults)
        {
            lGrid.AddRow(new Text[]
            {
                new Text(lEntry._id.CreationTime.ToLocalTime().ToString(), new Style(Color.Yellow)),
                new Text(lEntry._id.ToString(), new Style(Color.White)),
                new Text(lEntry.Completed.ToString(), new Style(lEntry.Completed ? Color.Green : Color.Red)),
                new Text(lEntry.Title, new Style(Color.Blue)),
                Constants.FormatPriorityText(lEntry.Priority),
                new Text(lEntry.Owner ?? string.Empty, new Style(Color.Yellow)),
                new Text(string.Join(", ", lEntry.Tags), new Style(Color.White))
            });
        }

        AnsiConsole.Write(lGrid);

        return 0;
    }
}