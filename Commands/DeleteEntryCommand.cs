﻿#region " Copyright Info "
// // Copyright (C) 2024 Over One Studio, LLC - All Rights Reserved
#endregion

using LiteDB;
using Spectre.Console;
using Spectre.Console.Cli;
using TaskDriver.Models;

namespace TaskDriver.Commands;

internal sealed class DeleteEntryCommand : Command<DeleteEntryCommand.Settings>
{
    internal sealed class Settings : CommandSettings
    {
        [CommandArgument(0, "[id]")]
        public string Id { get; set; }
    }

    private readonly ILiteDatabase _db;

    public DeleteEntryCommand(ILiteDatabase db)
    {
        _db = db;
    }

    public override int Execute(CommandContext context, DeleteEntryCommand.Settings settings)
    {
        if (string.IsNullOrWhiteSpace(settings.Id)) settings.Id = AnsiConsole.Ask<string>("[blue]What is the Id of your entry?[/]");
        settings.Id = Markup.Remove(settings.Id);

        if (string.IsNullOrWhiteSpace(settings.Id))
            return -1;

        var lEntries = _db.GetCollection<Entry>(nameof(Entry));

        var lResult = lEntries.Delete(new ObjectId(settings.Id));
        if (false == lResult)
        {
            AnsiConsole.MarkupLine($"[red]Entry not found:[/] [white]{settings.Id}[/]");
            return -1;
        }

        AnsiConsole.MarkupLine($"[blue]Deleted entry:[/] [white]{settings.Id}[/]");

        return 0;
    }
}