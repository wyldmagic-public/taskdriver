﻿// See https://aka.ms/new-console-template for more information

using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using Spectre.Console.Cli;
using TaskDriver.Commands;
using TaskDriver.DI;

if (false == Directory.Exists(Constants.AppProjectPath))
{
    Directory.CreateDirectory(Constants.AppProjectPath);
}

BsonMapper.Global.RegisterType<DateTimeOffset>
(
    serialize: obj =>
    {
        var doc = new BsonDocument();
        doc["DateTime"] = obj.DateTime.Ticks;
        doc["Offset"] = obj.Offset.Ticks;
        return doc;
    },
    deserialize: doc => new DateTimeOffset(doc["DateTime"].AsInt64, new TimeSpan(doc["Offset"].AsInt64))
);

var lRegistrations = new ServiceCollection();

lRegistrations.AddSingleton<ILiteDatabase, LiteDatabase>(x => new LiteDatabase(Constants.AppDatabasePath));

var lRegistrar = new Registrar(lRegistrations);
var lApp = new CommandApp(lRegistrar);

lApp.Configure(x =>
{
    x.AddCommand<InitCommand>("init")
        .WithDescription("Initializes or updates a TaskDriver project in the current directory")
        .WithExample("init", "Joshua")
        .WithExample("init", "Joshua", "joshua@example.com");

    x.AddCommand<AddEntryCommand>("add")
        .WithDescription("Adds a new entry to the TaskDriver project")
        .WithExample("add", "\"My first task\"")
        .WithExample("add", "\"My second task\"", "-o", "\"Joshua Allen\"")
        .WithExample("add", "\"My second task\"", "-p", "\"High\"")
        .WithExample("add", "\"My second task\"", "-t", "\"Tag One\"", "-t", "\"Tag Two\"");

    x.AddCommand<ListEntriesCommand>("list")
        .WithAlias("ls")
        .WithDescription("Lists or searches entries in the TaskDriver project")
        .WithExample("list")
        .WithExample("list", "-a")
        .WithExample("list", "-f", "\"First\"")
        .WithExample("list", "-o", "asc")
        .WithExample("list", "-t", "\"Tag One\"", "-t", "\"Tag Two\"");

    x.AddCommand<UpdateEntryCommand>("update")
        .WithAlias("up")
        .WithDescription("Updates an entry in the TaskDriver project")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-t", "\"My updated task title\"")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-c")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-o", "\"Joshua\"")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-p", "High")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-a", "\"Tag One\"", "-a", "\"Tag Two\"")
        .WithExample("update", "6675c8f73f59db0eb20bec16", "-r", "\"Tag One\"", "-a", "\"Tag Two\"");

    x.AddCommand<DeleteEntryCommand>("delete")
        .WithAlias("del")
        .WithDescription("Deletes an entry from the task driver project")
        .WithExample("delete", "6675c8f73f59db0eb20bec16");

    x.AddCommand<ShowDetailsCommmand>("details")
        .WithDescription("Shows a report of the TaskDriver project");
});

return lApp.Run(args);